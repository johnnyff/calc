/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src;

/**
 *
 * @author Lydia
 */
public class CalcModel implements CalcModelInterface {

    private final String expression;

    public CalcModel(String expression) {
        this.expression = expression;
    }

    public long calculate() {
        int operatorPlusIndex = getOperatorIndex("+", 0);
        int operatorMinusIndex = getOperatorIndex("-", 0);

        if (!doesOperatorExist(operatorPlusIndex) && !doesOperatorExist(operatorMinusIndex))
            return Long.valueOf(expression);

        OperatorModeler operatorModeler = new OperatorModeler(0, operatorPlusIndex, operatorMinusIndex);
        return getResult(operatorModeler);
    }

    private long getResult(OperatorModeler operatorModeler) {
        long result = 0;
        while (doesOperatorExist(operatorModeler.currentOperatorIndex)) {
            int nextOperatorIndex = getNextOperatorIndex(operatorModeler);

            result = getUpdatedResult(result, operatorModeler.currentOperatorIndex, nextOperatorIndex);

            operatorModeler = getUpdatedOperatorsIndexes(operatorModeler, nextOperatorIndex);
        }
        return result;
    }

    private int getNextOperatorIndex(OperatorModeler operatorModeler) {
        int nextOperatorIndex;
        if (isMinusOperatorNext(operatorModeler))
            if (doesOperatorExist(operatorModeler.operatorMinusIndex))
                nextOperatorIndex = operatorModeler.operatorMinusIndex;
            else
                nextOperatorIndex = operatorModeler.operatorPlusIndex;
        else if (isPlusOperatorNext(operatorModeler))
            if (doesOperatorExist(operatorModeler.operatorPlusIndex))
                nextOperatorIndex = operatorModeler.operatorPlusIndex;
            else
                nextOperatorIndex = operatorModeler.operatorMinusIndex;
        else
            nextOperatorIndex = -1;
        return nextOperatorIndex;
    }

    private long getUpdatedResult(long result, int currentOperatorIndex, int nextOperatorIndex) throws NumberFormatException {
        if (result == 0)
            result = getCalculatedResultsFromFirstOperation(currentOperatorIndex, nextOperatorIndex);
        else if (isPlusOperator(currentOperatorIndex))
            result = getCalculatedAdditionResults(result, currentOperatorIndex, nextOperatorIndex);
        else if (isMinusOperator(currentOperatorIndex))
            result = getCalculatedSubtractionResults(result, currentOperatorIndex, nextOperatorIndex);
        return result;
    }

    private long getCalculatedResultsFromFirstOperation(int currentOperatorIndex, int nextOperatorIndex) throws NumberFormatException {
        long result = 0;
        String operand_a = getOperand(0, currentOperatorIndex);
        String operand_b = getOperand(getStartIndexOfOperand(currentOperatorIndex), getEndIndexOfOperand(nextOperatorIndex));
        if (isPlusOperator(currentOperatorIndex))
            result = Long.valueOf(operand_a) + Long.valueOf(operand_b);
        else if (isMinusOperator(currentOperatorIndex))
            result = Long.valueOf(operand_a) - Long.valueOf(operand_b);
        return result;
    }

    private OperatorModeler getUpdatedOperatorsIndexes(OperatorModeler operatorModeler, int nextOperatorIndex) {
        if (doesOperatorExist(operatorModeler.operatorMinusIndex))
            operatorModeler.operatorMinusIndex = getOperatorIndex("-", operatorModeler.operatorMinusIndex + 1);
        if (doesOperatorExist(operatorModeler.operatorPlusIndex))
            operatorModeler.operatorPlusIndex = getOperatorIndex("+", operatorModeler.operatorPlusIndex + 1);
        operatorModeler.currentOperatorIndex = nextOperatorIndex;

        return operatorModeler;
    }

    private int getOperatorIndex(String operator, int startOfSearch) {
        return expression.indexOf(operator, startOfSearch);
    }

    private long getCalculatedSubtractionResults(long result, int currentOperatorIndex, int nextOperatorIndex) throws NumberFormatException {
        return result - Long.valueOf(getOperand(getStartIndexOfOperand(currentOperatorIndex), getEndIndexOfOperand(nextOperatorIndex)));
    }

    private long getCalculatedAdditionResults(long result, int currentOperatorIndex, int nextOperatorIndex) throws NumberFormatException {
        return result + Long.valueOf(getOperand(getStartIndexOfOperand(currentOperatorIndex), getEndIndexOfOperand(nextOperatorIndex)));
    }

    private String getOperand(int startIndexOfOperand, int endIndexOfOperand) {
        return expression.substring(startIndexOfOperand, endIndexOfOperand);
    }

    private int getStartIndexOfOperand(int operatorIndex) {
        return operatorIndex + 1;
    }

    private int getEndIndexOfOperand(int nextOperatorIndex) {
        return nextOperatorIndex == -1 ? expression.length() : nextOperatorIndex;
    }

    private boolean doesOperatorExist(int operatorIndex) {
        return operatorIndex != -1;
    }

    private static boolean isMinusOperatorNext(OperatorModeler operatorModeler) {
        return operatorModeler.operatorPlusIndex > operatorModeler.operatorMinusIndex;
    }

    private static boolean isPlusOperatorNext(OperatorModeler operatorModeler) {
        return operatorModeler.operatorMinusIndex > operatorModeler.operatorPlusIndex;
    }

    private boolean isPlusOperator(int currentOperatorIndex) {
        return expression.substring(currentOperatorIndex, currentOperatorIndex + 1).equals("+");
    }

    private boolean isMinusOperator(int currentOperatorIndex) {
        return expression.substring(currentOperatorIndex, currentOperatorIndex + 1).equals("-");
    }

    private class OperatorModeler {

        private int currentOperatorIndex;
        private int operatorPlusIndex;
        private int operatorMinusIndex;

        public OperatorModeler(int currentOperatorIndex, int operatorPlusIndex, int operatorMinusIndex) {
            this.currentOperatorIndex = currentOperatorIndex;
            this.operatorPlusIndex = operatorPlusIndex;
            this.operatorMinusIndex = operatorMinusIndex;

            initializeOperatorIndexes();
        }

        private void initializeOperatorIndexes() {
            if (isMinusOperatorNext(this))
                if (doesOperatorExist(operatorMinusIndex)) {
                    currentOperatorIndex = operatorMinusIndex;
                    operatorMinusIndex = getOperatorIndex("-", operatorMinusIndex + 1);
                } else {
                    currentOperatorIndex = operatorPlusIndex;
                    operatorPlusIndex = getOperatorIndex("+", operatorPlusIndex + 1);
                }
            else if (isPlusOperatorNext(this))
                if (doesOperatorExist(operatorPlusIndex)) {
                    currentOperatorIndex = operatorPlusIndex;
                    operatorPlusIndex = getOperatorIndex("+", operatorPlusIndex + 1);
                } else {
                    currentOperatorIndex = operatorMinusIndex;
                    operatorMinusIndex = getOperatorIndex("-", operatorMinusIndex + 1);
                }
        }
    }
}
