package test;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import src.CalcModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author john
 */
public class CalcModelTest {
    
    @Test
    public void testCalcSomeSingleValue() {
        String expression = "2";
        CalcModel calcModel = new CalcModel(expression);
        long actualValue = calcModel.calculate();
        long expectedValue = 2;
        Assertions.assertEquals(expectedValue, actualValue);
    }
    
    @Test
    public void testCalcSomeOtherSingleValue() {
        String expression = "3";
        CalcModel calcModel = new CalcModel(expression);
        long actualValue = calcModel.calculate();
        long expectedValue = 3;
        Assertions.assertEquals(expectedValue, actualValue);
    }
    
    @Test
    public void testCalcAddTwoSingleDigits() {
        String expression = "2+3";
        CalcModel calcModel = new CalcModel(expression);
        long actualValue = calcModel.calculate();
        long expectedValue = 5;
        Assertions.assertEquals(expectedValue, actualValue);
    }
    
    @Test
    public void testCalcAddThreeSingleDigits() {
        String expression = "2+3+4";
        CalcModel calcModel = new CalcModel(expression);
        long actualValue = calcModel.calculate();
        long expectedValue = 9;
        Assertions.assertEquals(expectedValue, actualValue);
    }
    
    @Test
    public void testCalcAddFourSingleDigits() {
        String expression = "1+2+3+4";
        CalcModel calcModel = new CalcModel(expression);
        long actualValue = calcModel.calculate();
        long expectedValue = 10;
        Assertions.assertEquals(expectedValue, actualValue);
    }
    
    @Test
    public void testCalcAddTwoMultipleDigits() {
        String expression = "21+13";
        CalcModel calcModel = new CalcModel(expression);
        long actualValue = calcModel.calculate();
        long expectedValue = 34;
        Assertions.assertEquals(expectedValue, actualValue);
    }
    
    @Test
    public void testCalcAddThreeMultipleDigits() {
        String expression = "21+13+10";
        CalcModel calcModel = new CalcModel(expression);
        long actualValue = calcModel.calculate();
        long expectedValue = 44;
        Assertions.assertEquals(expectedValue, actualValue);
    }
    
    @Test
    public void testCalcSubtractTwoSingleDigits() {
        String expression = "2-3";
        CalcModel calcModel = new CalcModel(expression);
        long actualValue = calcModel.calculate();
        long expectedValue = -1;
        Assertions.assertEquals(expectedValue, actualValue);
    }
    
    @Test
    public void testCalcSubtractThreeSingleDigits() {
        String expression = "2-3+5";
        CalcModel calcModel = new CalcModel(expression);
        long actualValue = calcModel.calculate();
        long expectedValue = 4;
        Assertions.assertEquals(expectedValue, actualValue);
    }
    
    @Test
    public void testCalcSubtractTwoMultipileDigits() {
        String expression = "12-30";
        CalcModel calcModel = new CalcModel(expression);
        long actualValue = calcModel.calculate();
        long expectedValue = -18;
        Assertions.assertEquals(expectedValue, actualValue);
    }
    
    @Test
    public void testCalcSubtractThreeMultipileDigits() {
        String expression = "12-30-10";
        CalcModel calcModel = new CalcModel(expression);
        long actualValue = calcModel.calculate();
        long expectedValue = -28;
        Assertions.assertEquals(expectedValue, actualValue);
    }
}
